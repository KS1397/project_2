#ifndef HEADER_FILE
#define HEADER_FILE
#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <fstream>



class Node{
    
    public:
    
    std::string title;
    std::string author;
    std::string isbn;
    int qty;
    
    Node* next;
};

class LinkedList{
Node* head;

public:

LinkedList();
~LinkedList();
void search();
void add(Node * temp);
int remove();
void load(std::string fn);
void loadAnother();
void save();


};
#endif

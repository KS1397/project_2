#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include "Books.h"
#include<stdio.h>
#include<stdlib.h>


LinkedList::LinkedList()
{
    
  head=new Node();  

}
LinkedList::~LinkedList()
{
    Node* curr = head;
    while(curr!=NULL)
    {
        Node *temp=curr;
        curr=curr->next;
        delete temp; 
    }

}
void LinkedList::search()
{   
    std::string tit;
    Node* t = head;
    if (t == NULL) {
        std::cout << "No Books Available\n";
    }
    // else {
    //     std::cout << "BOOKS\t\n";
  
    //     // Until list is not NULL
    //     while (t != NULL) {
    //         std::cout << t->title <<std::endl;
    //              t = t->next;
    //     }
    //     std::cout<<std::endl;
    // }
    std::cout << "Enter the Title of Book to Search\n";
    std::cin.ignore();
    getline(std::cin,tit);
    // if head is NULL
    if (!head) {
        std::cout << "No Books Avialable\n";
        return;
    }
  
    
    else {
        Node* p = head;
        
        while (p) {
            if (p->title == tit ) {
                std::cout << "Title: "<<p->title << std::endl;
                std::cout << "Author(s): "<<p->author << std::endl;
                std::cout << "ISBN: "<<std::setprecision(10)<<p->isbn << std::endl;
                std::cout << "Quantity: "<<std::setprecision(5)<<p->qty << std::endl;
                return;
            }
            p = p->next;
        }
        if (p == NULL)
            std::cout << "No such Book Avialable\n";
  
        
    }

}
void LinkedList::add(Node * temp)
{
    
        
            // Insert at Begin
            if (head == NULL ) {
                temp->next = head;
                head = temp;
            }
        
            // Insert elsewhere
            else {
                Node* temp2 = head;
                while (temp2->next != NULL ) 
                {
                    temp2 = temp2->next;
                }
                
                temp->next = temp2->next;
                temp2->next = temp;
            }
        
        


            

}
void LinkedList::save()
{
    Node *temp = head;
    std::ofstream pFile;
    
    pFile.open("books.txt");
    temp = temp->next;
    pFile<<temp->title<<"\t"<<temp->author<<"\t"<<temp->isbn<<"\t"<<temp->qty<<"\t";
    temp = temp->next; 
         while(temp)
          {
              if (pFile.is_open())
              {
                 pFile<<std::endl; 
                 pFile<<temp->title<<"\t"<<temp->author<<"\t"<<temp->isbn<<"\t"<<temp->qty<<"\t";

                
              }
              temp=temp->next;
          }
        pFile.close();

    


}

int LinkedList::remove()
{
    Node* t = head;
    Node* p = NULL;
    std::string lnode; 
    std::string tit;
    int qt;
    std::cout << "Enter the Number of Books lost/damaged.\n";
    std::cin>>qt;
    std::cout << "Enter the Title of Book\n";
    std::cin.ignore();
    getline(std::cin,tit);
    // Deletion at Begin
    if (t != NULL && t->title == tit) {
        
            t->qty=t->qty-qt;
            std::cout<<"Book qty reduced\n";
        if(t->qty<=0)
        {
            head = t->next;
            delete t;
            std::cout << "Book removed Successfully\n";
        
        }


        
        return 0;
    }
    
    // Deletion Other than Begin
   else
   { 
       while (t != NULL && t->title != tit) 
       {
        p = t;
        t = t->next;
       }

        if (t == NULL) 
        {
            std::cout << "Book does not Exist\n";
            return -1;
        }
        else
        { 
            t->qty=t->qty-qt;
            std::cout<<"Boon qty reduced"<<std::endl;

            if(t->qty<=0)
            { 
            p->next = t->next;
    
            delete t;
            std::cout << "Book removed Successfully\n";
    
            }    

            
    
        }

    }
    return 0;
}

void LinkedList::load(std::string fn)
{
    
    std::ifstream infile;
    infile.open(fn);
	
    char *data =new char[500]; 
	
	if(infile.is_open())
    {   
          
        while(!infile.eof()){   
        
        Node *temp = new Node();
        infile.getline (data,500);

        char delim[] = "\t";
        char *token = strtok(data,delim);
        temp->title=token;
        token = strtok(NULL,delim);
        temp->author=token;        
        token = strtok(NULL,delim);
        temp->isbn=token;
        token = strtok(NULL,delim);
        temp->qty=atof(token);  


        temp->next=NULL;
        
        add(temp);
        
        } 
       infile.close();
    }
}

void LinkedList::loadAnother()
{

    std::string filename;
    std::ifstream infile;
    std::cout<<"Enter the file to Load books from e.g (filename.txt)."<<std::endl;
    std::cin>>filename;
    infile.open(filename);
	
    char *data =new char[500]; 
	
	if(infile.is_open())
    {   
          
        while(!infile.eof()){   
        
        Node *temp = new Node();
        infile.getline (data,500);

        char delim[] = "\t";
        char *token = strtok(data,delim);
        temp->title=token;
        token = strtok(NULL,delim);
        temp->author=token;        
        token = strtok(NULL,delim);
        temp->isbn=token;
        token = strtok(NULL,delim);
        temp->qty=atof(token);


        temp->next=NULL;
        
        add(temp);
        
        } 
       infile.close();
    }

}

int main(int argc, char *argv[])
{
    
 LinkedList abc ;
  abc.load(argv[1]);
    // Menu
    while (true) {
        
        std::cout << "\n\t\tLibrary\n\n\tPress\n\t1 to Add a new Book\n\t2 to Search a Book\n\t3 to Report Lost/Damaged   "
                "Book\n\t4 to Load another file \n\t5 to Exit";
        std::cout << "\nEnter your Choice\n";
        int Choice;
  
        // Enter Choice
        std::cin >> Choice;
        if (Choice == 1) 
        {
                std::string tit;
                std::string aut;
                std::string isb;
                
                int qt;
                        
            std::cout << "Enter the Title of Book\n";
            std::cin.ignore();
            getline(std::cin,tit);
            
            std::cout << "Enter the Author of Book\n";
            getline(std::cin,aut);
            
            std::cout << "Enter the ISBN of Book\n";
            getline(std::cin,isb);
            std::cout << "Enter  the Quantity of Book\n";
            std::cin >> qt;

            if(qt==0)
            {
                std::cout<<"Quantity cannot be zero(0):\n";
                continue;

            }

            // Create new Node to Insert Record
            Node* temp = new Node();
            temp->title = tit;
            temp->author = aut;
            temp->isbn = isb;
            temp->qty = qt;
            temp->next = NULL;
            
            
            
            
            
            abc.add(temp);  
            std::cout<<"Book Added\n"; 
            abc.save();
        }
        else if (Choice == 2) {
            abc.search();
        }
        else if (Choice == 3) {
            abc.remove();
            abc.save();
        }
        
        else if (Choice == 4) {
            abc.loadAnother();
        }

        else if (Choice == 5) {
            return 0;
        }
        
        else {
            std::cout << "Invalid Choice Try Again\n";
        }
    }

    return 0;
}
